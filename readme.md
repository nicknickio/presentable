# Laravel view presenters.

Ever had those scenarios where you needed to make __Clumpsy__ code in your models?

## Installation
Pull it in through composer.json

```js
{
    "require": {
        "NickNick/Presentable": "1.0.*"
    }
}
```

## Usage

I personally prefer to keep the __Presenters__ within **App\Presenters**.

```php
namespace App\Presenters;
use NickNick\Presentable\Presenter;

class UserPresenter extends Presenter
{

    public function fullName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

}
```

Next up, you need to define the presenter you want to use in a specific model.

```php
use NickNick\Presentable\PresentableTrait;
use App\Presenters\UserPresenter;

class User extends Model
{
    
    use PresentableTrait;

    protected $presenter = UserPresenter::class;

    # Relations here..

}
```

Thats it, we're done.
```php
<h1>Hello, {{ $user->present()->fullName }}</h1>
```